import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-basic',
  templateUrl: './basic.component.html',
  styleUrls: ['./basic.component.scss']
})
export class BasicComponent implements OnInit {

  diffrent = [
    {
      id: 1,
      angularjs : 'It is based on MVC architecture',
      angular : 'This is based on Service/Controller'
    },
    {
      id : 2,
      angularjs : 'It uses JavaScript to build the application',
      angular :  'Introduced the TypeScript to write the application'
    },
    {
      id : 3,
      angularjs : 'Based on controllers concept',
      angular :  'This is a component based UI approach'
    },
    {
      id : 4,
      angularjs : 'Not a mobile friendly framework',
      angular :  'Developed considering mobile platform '
    },
    {
      id : 5,
      angularjs : 'Difficulty in SEO friendly application development	',
      angular : 'Ease to create SEO friendly applications'
    }
  ]

   displaydata :  string = 'import { platformBrowserDynamic } from ‘@angular/platform-browser-dynamic’;'
  constructor() { }

  ngOnInit(): void {
  }

}
