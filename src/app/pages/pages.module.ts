import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagesRoutingModule } from './pages-routing.module';
import { PagesComponent } from './pages.component';
import { FooterComponent } from '../component/footer/footer.component';
import { HeaderComponent } from '../component/header/header.component';
import { SidebarComponent } from '../component/sidebar/sidebar.component';


@NgModule({
  declarations: [PagesComponent, HeaderComponent, FooterComponent, SidebarComponent],
  imports: [
    CommonModule,
    PagesRoutingModule
  ],
  exports: [HeaderComponent, FooterComponent, SidebarComponent]
})
export class PagesModule { }
